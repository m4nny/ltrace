#ifndef ELFINFO
# define ELFINFO

# include <stdlib.h>
# include <stdint.h>
# include <stdbool.h>
# include <errno.h>
# include <stdio.h>
# include <unistd.h>
# include <string.h>
# include <assert.h>
# include <fcntl.h>
# include <elf.h>

# include <utils/tools.h>

struct section
{
    Elf64_Shdr* header;
    void* content;
};


struct elf
{
    // access to file
    uint32_t fd;

    // basic headers and header tables
    Elf64_Ehdr* ehdr;
    Elf64_Shdr* shtbl;

    // various re-usable sections
    Elf64_Sym* dynsym;
    char* shstrtab;
    struct section dynstr;
};


// open a file descriptor to the binary, initialize
// elf structure, load basic elf headers and return structure
struct elf*
elf_init(const char* filename);

// free all memory allocated into the elf structure
void
elf_destroy(struct elf* elf);

// return a structure containing both a pointer to the section header and
// another to the section content.
struct section
elf_get_section(struct elf* elf, const char* section_name, bool dist);

// free the memory allocated by the elf_get_section() function appropriately
void
elf_free_section(struct section sec);

// get dynamic symbol name at index 'index' in the dynsym section
char*
elf_get_dynsymbol(struct elf* elf, uint32_t index, bool distant);

// free the dynamic symbol name allocted by the elf_get_dynsymbol function
void
elf_free_dynsymbol(char* symbol);


#endif // !ELFINFO
