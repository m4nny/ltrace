#include <utils/elfinfo.h>


static bool
_elf_check(uint32_t fd)
{
    // save old offset to restore after function call
    uint32_t old_off = lseek(fd, 0, SEEK_CUR);
    lseek(fd, 0, SEEK_SET);

    // check with first 4 bytes
    char sink[5] = { 0 };
    read(fd, sink, 4);

    // restore old offset
    lseek(fd, old_off, SEEK_SET);
    return strcmp(sink, "\177ELF");
}


static void
_elf_load_ehdr(struct elf* elf)
{
    if (elf->ehdr != NULL)
        return;

    // initialize elf header struct
    elf->ehdr = malloc(sizeof (Elf64_Ehdr));
    lseek(elf->fd, 0, SEEK_SET);
    read(elf->fd, elf->ehdr, sizeof (Elf64_Ehdr));

    // verify the integrity of the struct we just loaded
    assert(sizeof(Elf64_Ehdr) == elf->ehdr->e_ehsize);
    assert(sizeof(Elf64_Shdr) == elf->ehdr->e_shentsize);
}


static void
_elf_load_shtbl(struct elf* elf)
{
    if (elf->shtbl != NULL)
        return;

    lseek(elf->fd, elf->ehdr->e_shoff, SEEK_SET);
    elf->shtbl = malloc(elf->ehdr->e_shentsize * elf->ehdr->e_shnum);
    read(elf->fd, elf->shtbl, elf->ehdr->e_shentsize * elf->ehdr->e_shnum);
}


static void
_elf_load_shstrtab(struct elf* elf)
{
    if (elf->shstrtab != NULL)
        return;
    assert(elf->shtbl[elf->ehdr->e_shstrndx].sh_type == SHT_STRTAB);

    uint64_t size = elf->shtbl[elf->ehdr->e_shstrndx].sh_size;
    elf->shstrtab = malloc(size);
    lseek(elf->fd, elf->shtbl[elf->ehdr->e_shstrndx].sh_offset, SEEK_SET);
    read(elf->fd, elf->shstrtab, size);
}


static void
_elf_load_dynstr(struct elf* elf)
{
    if (elf->dynstr.content != NULL)
        return;

    elf->dynstr = elf_get_section(elf, ".dynstr", 0);
}


static void
_elf_load_dynsym(struct elf* elf)
{
    if (elf->dynsym != NULL)
        return;

    struct section dynsym = elf_get_section(elf, ".dynsym", 0);
    elf->dynsym = dynsym.content;
}


struct elf*
elf_init(const char* filename)
{
    uint32_t fd = -1;
    if ((fd = open(filename, O_RDONLY)) == -1 || _elf_check(fd) != 0)
        return NULL;

    struct elf* elf = malloc(sizeof (struct elf));
    if (elf == NULL)
        return NULL;

    memset(elf, 0, sizeof (struct elf));

    elf->fd = fd;
    _elf_load_ehdr(elf);
    _elf_load_shtbl(elf);

    return elf;
}


void
elf_destroy(struct elf* elf)
{
    if (elf != NULL)
    {
        if (elf->ehdr != NULL)
            free(elf->ehdr);
        if (elf->shtbl != NULL)
            free(elf->shtbl);
        if (elf->shstrtab != NULL)
            free(elf->shstrtab);
        if (elf->dynsym != NULL)
            free(elf->dynsym);

        elf_free_section(elf->dynstr);
        close(elf->fd);
        free(elf);
    }
}


struct section
elf_get_section(struct elf* elf, const char* section_name, bool dist)
{
    struct section sec = { .header = NULL, .content = NULL };

    // load shstrtab to compare section names
    _elf_load_shstrtab(elf);
    for (uint8_t i = 0; i < elf->ehdr->e_shnum; i += 1)
    {
        // if this section is the one we're looking for
        if (strcmp(elf->shstrtab + elf->shtbl[i].sh_name, section_name) == 0)
        {
            // initialize section structure
            sec.header = &elf->shtbl[i];

            if (!dist)
            {
                sec.content = malloc(elf->shtbl[i].sh_size);
                lseek(elf->fd, elf->shtbl[i].sh_offset, SEEK_SET);
                read(elf->fd, sec.content, elf->shtbl[i].sh_size);
            }
            else
                sec.content = NULL;
        }
    }

    return sec;
}


void
elf_free_section(struct section sec)
{
    if (sec.content != NULL)
        free(sec.content);
}


char*
elf_get_dynsymbol(struct elf* elf, uint32_t index, bool distant)
{
    _elf_load_dynstr(elf);
    _elf_load_dynsym(elf);

    if (distant)
    {
        uint64_t ret = elf->dynsym[index].st_name;
        return (char*) ret;
    }

    return mlt_strdup((char*)elf->dynstr.content + elf->dynsym[index].st_name);
}


void
elf_free_dynsymbol(char* symbol)
{
    free(symbol);
}
