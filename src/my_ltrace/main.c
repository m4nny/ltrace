#include <elf.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/user.h>

#include <utils/breakpoints.h>
#include <utils/elfinfo.h>
#include <utils/tools.h>
#include <utils/conf.h>

#include <my_ltrace/display.h>


static void
_resume(pid_t child, struct user_regs_struct* regs, struct bp* bp, int32_t* st)
{
    // restore rip pointer and original instruction
    regs->rip -= 1;
    ptrace(PTRACE_SETREGS, child, NULL, regs);
    ptrace(PTRACE_POKEDATA, child, bp->plt_addr, bp->instr);

    // re-insert only if PLT breakpoint (has symbol and stuff)
    if (bp->symbol_name != NULL)
    {
        // single-step over it before re-inserting breakpoint
        ptrace(PTRACE_SINGLESTEP, child, NULL, NULL);
        waitpid(child, st, 0);
        bp_insert(child, bp);
    }

    // resume execution
    ptrace(PTRACE_CONT, child, NULL, NULL);
    waitpid(child, st, 0);
}


static bool
_ugly_trick(pid_t child, struct user_regs_struct* regs, struct bp* bp,
    int32_t* status)
{
    static const char* avoid[2] = { "__libc_start_main", NULL };
    char** tmp = (char**) avoid;

    while (*tmp != NULL && strcmp(bp->symbol_name, *tmp) != 0)
        tmp += 1;

    if (*tmp != NULL)
        _resume(child, regs, bp, status);
    return *tmp != NULL;
}

static void
_loop(char* argv[], struct fct* fcts, struct bp* bps, uint32_t bpcount)
{
    pid_t child;

    if ((child = fork()) == 0)
    {
        ptrace(PTRACE_TRACEME, 0, NULL, NULL);
        execvp(argv[1], argv + 1);
    }
    else
    {
        int32_t status;
        waitpid(child, &status, 0);

        // save instruction at address bps[i].plt_addr and replace with int3
        for (uint32_t i = 0; i < bpcount; i += 1)
            bp_insert(child, bps + i);
        ptrace(PTRACE_CONT, child, NULL, NULL);

        waitpid(child, &status, 0);
        while (!WIFEXITED(status))
        {
            // study state of child (regs, ret val, current breakpoint..)
            struct user_regs_struct regs;
            ptrace(PTRACE_GETREGS, child, NULL, &regs);
            struct bp* cur = bp_find((void*) (regs.rip - 1), bps, bpcount);

            // ugly trick to avoid the non-returning functions
            if (_ugly_trick(child, &regs, cur, &status))
                continue;

            // place breakpoint on return address
            struct bp ret = { .plt_addr = peek_word(child, regs.rsp) };
            ret.symbol_name = NULL;

            // print function call
            display_proto(child, &regs, cur->symbol_name, fcts);

            // setup breakpoint at return address for further analyze
            // restore ip pointer and instruction, ss, reinsert bp and go on
            bp_insert(child, &ret);
            _resume(child, &regs, cur, &status);

            // stop at function return address and retrieve info
            ptrace(PTRACE_GETREGS, child, NULL, &regs);

            // display return value and library name
            display_ret(child, &regs);

            // resume execution onto next library call
            _resume(child, &regs, &ret, &status);
        }
    }
}


int main(int argc, char* argv[])
{
    if (argc < 2)
        return 2;

    // open elf file and load basic headers
    struct elf* elf = elf_init(argv[1]);
    if (elf == NULL)
        return 1;

    // load for library calls in the elf file and get the future breakpoints
    uint32_t bps_count = 0;
    struct bp* bps = bp_get(elf, &bps_count, 0);
    if (bps == NULL || bps_count == 0)
        return 1;

    // open configuration file and get a list of function prototypes
    struct fct* prototypes = conf_parse_file("etc/trace.conf");

    // main execution function
    _loop(argv, prototypes, bps, bps_count);

    // free all that stuff
    conf_free_fct(prototypes);
    bp_free(bps, bps_count, 0);
    elf_destroy(elf);
    return 0;
}
