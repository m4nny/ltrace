#ifndef DISPLAY
# define DISPLAY

# include <stdio.h>
# include <string.h>
# include <sys/user.h>
# include <sys/types.h>

# include <utils/conf.h>
# include <utils/tools.h>


void
display_proto(pid_t child, struct user_regs_struct* regs, const char* sym,
    struct fct* prototypes);

void
display_ret(pid_t child, struct user_regs_struct* regs);


#endif // !DISPLAY
