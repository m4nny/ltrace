#ifndef SETUP
# define SETUP

# include <sys/wait.h>
# include <sys/user.h>
# include <sys/mman.h>
# include <fcntl.h>

# include <utils/breakpoints.h>


uint32_t
setup_tmp_file(struct bp* bps, uint32_t bps_count);

void*
setup_distant(pid_t child, struct bp* bp, uint32_t fd, uint32_t* status);

uint32_t
setup_child(const char* filename);

uint8_t
setup_stubs(pid_t child, void* chunk, struct bp* bps, uint32_t bps_count);


#endif // !SETUP
